[![pipeline status](https://gitlab.com/ykcab/almb.me/badges/master/pipeline.svg)](https://gitlab.com/ykcab/almb.me/commits/master)

# blog

* [Run locally](README.md#run-locally)
* [License](README.mb#licese)

This is the offical repository of my blog almb.me.
It's hosted right here with GitLab Pages. Submit a PR for any typo or improvement.

### Run locally

1. Install [hugo](https://gohugo.io/)

1. Run `hugo serve`

### License

[MIT](./LICENSE) &copy; alain