---
title: "Building My Ultimate Driving Machine - The 2"
date: 2021-12-19T23:31:56-05:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["lifestyle","car build"]
categories: ["general"]

lightgallery: true
---

## Let's Begin

This project is a shift break from security engineering. It focuses on major work being done to making my [Ultimate driving machine](https://www.hotcars.com/secret-origin-bmws-ultimate-driving-machine-slogan/). BMW prides itself as a conventional unique way to experience their masterpieces.  

Every manufacturer has an entry level model that juices you up to wanting an upgraded version. While most average consumers would go this route, that is not the case for a car enthousiate like myself and you. The moment you're into the car modifying world, that's it. You will sink in and never come out of the trap easily.

Below is a list of all modifications done and/or yet to be installed to transform The 2 [2015 228i RWD N20](youtube.com/@hyde_official) to an Ultimate Driving Machine yet streeet legal, reliable yet outperform every model of its cotegory. Some modifications link to a DIY video, enjoy and don't forget to subscribe.

## Performance Ugrades and Modifications

### Powertrain - Engine and its components

* Closed Deck Conversion Aka Engine Rebuild
   * [800HP N20 Build](https://www.youtube.com/watch?v=7rWPXdYIFP4&list=PL8xD5jrgH96spG9Zs1LjFK0HhtX65YxVw&pp=iAQB)
* [Bootmod3 Stage 1 93 octane](https://www.youtube.com/watch?v=bDbNtfGjz8s)

* [1-step colder ignition coil setup](https://youtu.be/sQStVD86jHc)

* [Water Methanol (WMI)](https://youtu.be/U6RQIX8z3MM) - Note: Ultimately I wanted a system with a failsafe mechanism. The failsafe mechanism protects the engine in case of WMI failures. Currently bm3 maps for N20 don't support it yet, but What I planned in the installation is to have an ON/OFF switch. Assuming I am runing low on meth while cruising, I will tun it off and load a safer BM3 map that doesn't use the WMI system. the Video explains it well.

### Drivetrain

* [Cosmos short shifter](https://youtu.be/momwVpFwGzA)

### Suspension

* [Full suspension upgrade](https://youtu.be/E5rP9lw7Ovc?feature=shared)
  * Coilovers
  * Street Performance Rear sway bar
  * Street Performance Front sway bar
  * Full refresh of front control arms
  * Full refresh of rear control arms with Adjustable control arms


### Brake System

* [Big Brake Kit (installation)](https://youtu.be/2EpBBqEyZ0M)

|              | Rims        |Tires                |Rotors       |Calipers
|:-------------|:------------|:--------------------|:------------|:-------
|Front         |18" 8.5      |245/40 97Y           |355mm 2-piece|GT6 (6-pots piston)
|Rear          |18" 8.5      |245/40 97Y           |345mm 2-piece|GT4 (4-Pots piston)

Rims Make and Model: BMW 763M Replica  
Tires Make & Model: [Crosswind All-Season](https://amzn.to/3ZJLJNt)

## Cosmetic Ugrades and Modifications

Here is a list of cosmetic mod done thus far:

* [M2CS Full Body Kit](https://youtu.be/tiBfb3kL-Tw)
* [Custom ergnomic stearing wheel](https://youtu.be/lB9NHMKmgkE)
* [Instrument Cluster LED Swap](https://youtu.be/IZXINWavKCU)

* [Black Gloss Kidney Grill](https://youtu.be/wrDxEAklXbo)

* [Chrome Delete](https://youtu.be/hMiUzJxpdCg)

* [Aluminum Shell Forged Oil Filter Housing](https://youtu.be/V8fhM8RqQsM)

* Retrofited the red start/stop button

* Custom Headlight Angel Eyes

* Blacked out Headlight: Painted black all chrome parts of the headlight housing.
