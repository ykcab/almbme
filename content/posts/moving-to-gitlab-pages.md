---
title: "Moving to Gitlab Pages"
date: 2019-06-14T08:32:01-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["git","gitlab","hugo"]
categories: ["Engineering"]

lightgallery: true
---
_DISCLAIMER: All views expressed on this site are my own and do not represent the opinions of any entity whatsoever with which I have been, am now, or will be affiliated. THIS POST HAS NO AFFILIATION TO ANY ADVERTISEMENT._

Finaly this blog is live and happy to have completed the project. I will try to keep this short and precise. Previously, all my creative contents were published on Medium, however, the choice to move off of Medium was mainly to have some flexibility as well as put my technical skills into some challenges. Another motivation factor is lack of support of [custom domain](https://help.medium.com/hc/en-us/articles/115003053487-Custom-Domains-service-deprecation). And last but not least of why this decision was made is linked to Medium's business model which to place creators' contents behind a paywall. At first, I agreed to the idea, but then it turns out into a direction I do not feel to be fair. More importantly, there have been concerns on that matter as you can read [here](https://kevin.lexblog.com/2019/06/08/mediums-paywall-ends-distribution-of-content-for-legal-professionals/) and [here](https://3lionsweb.com/news/freecodecamp-moves-off-of-medium-after-being-pressured-to-put-articles-behind-paywalls/). At the end, it's someone else's platform.

This move benefits not only how it liberates me from any constraints that are set onto those platforms but also opens up endless opportunities and ideas, plus it will avoid situations like [this](https://twitter.com/KarelDonk/status/1134525892863102981).

Alright, let talk about services that power this blog. I did some due diligence, and came up with this alternative solution since its offers an easy transition from Medium. The best solution I could find was to use GitLab Pages and Cloudflare. It fit the need of everything for a static site. These two technologies offer a seamless UX (User Experience) to Medium.  
Many already know about Cloudflare, and may or may not heard of Gitlab Pages. GitLab pages is similar to Github pages which is a feature that allows you to publish static websites directly from a repository in GitLab. You can use it either for personal or business websites, such as portfolios, documentation, manifestos, and business presentations. You can also attribute any license to your content [(from the official site)](https://docs.gitlab.com/ee/user/project/pages/). Additionally, the blog is beautified and power by [gohugo](https://gohugo.io/).

To sum up, the possibilities of owning and self-hosting your blog, whether hosting with GitLab Pages or AWS is limitless. Something to note here is that it's cost saving except your time, and I'd recommend to try it out.

Thank you - If you liked what you read, support by sharing this, submit a pull request for any suggestions or questions.  
