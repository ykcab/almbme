---
title: "Summer Cryptography Journal 2021: RSA Security Cryptology Paper"
date: 2021-04-30T18:31:37-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["cryptography"]
categories: ["Security Engineering"]

lightgallery: true
---
In summer of last year (2020), [I published a handful of cypto resources](https://almb.me/post/crypto-journey/) I put togather as I was working on building a secure file sharing service, [YBITS.IO](https://ybits.io/info).
This summer (2021), I focussed on the history of RSA. What I discovered is incredible and I wrote is as part of the reseach paper for my school propram. I thought it's a very good resource I'd like everyone to read the part which I call the state of security of RSA.
Engjoy the reading by downloading the PDF [here](../../assets/rsa_security_cryptology_paper.pdf)
