---
title: "Generate SAN CSR With OpenSSL"
date: 2019-06-16T19:21:17-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["RSA","SAN CSR", "DNS"]
categories: ["Engineering"]

lightgallery: true
---

When buying a TLS/SSL certificate from a CA, mainly a user supplies a csr file. The easiest way is using the OpenSSL CLI. But the tricky part lies in generating a SAN CSR. Although users can scope it in one liner, better to not mess with system config. Therefore the following method mitigates that.  
Copy this code block, and save it as `san.conf` or anything of preference (change parameters accordingly):

```
[ req ]
default_bits       = 4096
prompt             = no
encrypt_key        = yes
default_md         = sha256
distinguished_name = dn
req_extensions     = req_ext

[ dn ]
CN = <fqdn>
O  = org
OU = org_unit
L  = local_city
ST = the_state
C  = US

[ req_ext ]
subjectAltName = @alt_names

[alt_names]
DNS.1=<fqdn>
DNS.2=<fqdn>
DNS.3=<fqdn>
```

Run this command from the same directory as `san.conf` file:  
```openssl req -new -config san.conf -keyout domain.key -out domain.csr```
You can add as many domains as possible under `[alt_names]` section. For more command references, check out [this link](https://www.sslshopper.com/article-most-common-openssl-commands.html).
