---
title: " Understanding SSL/TLS - A Non-Technical Guide"
date: 2023-01-29T08:42:34-05:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["Networking", "tls", "security","browser security"]
categories: ["Engineering"]

lightgallery: true
---

When transmitting sensitive data over the internet, it's important to ensure that the data remains confidential and integrity is maintained. One of the ways to achieve this is by using SSL/TLS (Secure Sockets Layer/Transport Layer Security) protocols. In this blog post, we'll discuss what SSL/TLS is, how it works, and its components.

## What is SSL/TLS?

When transmitting sensitive data over the internet, it's important to ensure that the data remains confidential and integrity is maintained. One of the ways to achieve this is by using SSL/TLS (Secure Sockets Layer/Transport Layer Security) protocols. In this blog post, we'll discuss what SSL/TLS is, how it works, and its components.
What is SSL/TLS?

SSL (Secure Sockets Layer) was a security protocol developed in the 1990s to provide secure communications over the internet. However, the protocol has since been deprecated and replaced by its successor, TLS (Transport Layer Security). Today, the terms SSL and TLS are often used interchangeably to refer to the same protocol.

SSL/TLS is a cryptographic protocol that provides secure communications between a client and a server over the internet. It works by encrypting the data transmitted between the client and server, ensuring that the data remains confidential and integrity is maintained, even if the data is intercepted in transit.

## How does SSL/TLS work?

The SSL/TLS handshake is a multi-step process that establishes a secure connection between a client and a server. The steps involved in the SSL/TLS handshake are:

   1. Client sends a "ClientHello" message, which includes the client's SSL/TLS version number, a randomly generated number, and a list of supported cipher suites.

   1. Server responds with a "ServerHello" message, which includes the server's SSL/TLS version number, the same randomly generated number sent by the client, and the chosen cipher suite.

   1. The server sends its SSL/TLS certificate, which includes its public key, to the client.

   1. The client verifies the certificate, checking that it was issued by a trusted certificate authority (CA) and that it has not been revoked.

   1. The client generates a "pre-master secret" and encrypts it with the server's public key.

   1. The server decrypts the pre-master secret using its private key.

   1. The client and server use the pre-master secret to generate a "master secret", which is then used to generate session keys for encrypting and decrypting data.

   1. The client sends a "ClientFinished" message to the server, indicating that the handshake is complete.

   1. The server sends a "ServerFinished" message to the client, indicating that the handshake is complete and the secure connection is established.

From this point on, the client and server use the session keys to encrypt and decrypt the data they exchange, ensuring the confidentiality and integrity of the data.

## SSL/TLS Certificates

An SSL/TLS certificate is a digital certificate that contains the public key of a website and information about the identity of the website. The certificate is issued by a trusted certificate authority (CA), which verifies the identity of the website before issuing the certificate.

When a client visits a website that has an SSL/TLS certificate, the client's browser verifies the certificate before establishing a secure connection with the server. If the certificate is valid and has been issued by a trusted CA, the browser establishes a secure connection and displays a padlock icon in the address bar to indicate that the connection is secure.

## Encryption Algorithms in SSL/TLS

The recommended encryption algorithm for creating an SSL/TLS certificate depends on the client and server software and the desired level of security. However, here are some of the commonly recommended encryption algorithms:

1. RSA (Rivest–Shamir–Adleman)
1. Elliptic Curve Cryptography (ECC)
1. Advanced Encryption Standard (AES)

AES (Advanced Encryption Standard) is a symmetric key encryption algorithm that is commonly used to encrypt data in SSL/TLS. In symmetric key encryption, the same key is used to encrypt and decrypt the data, making it fast and efficient. AES is considered to be one of the strongest encryption algorithms available and is widely used in various applications, including SSL/TLS.


## What is SSL/TLS and How Does it Work?

SSL (Secure Sockets Layer) and TLS (Transport Layer Security) are cryptographic protocols that provide secure communications over the internet. They are widely used to secure websites, email, and other types of internet traffic. When a client (such as a web browser) connects to a server (such as a web server), the SSL/TLS protocol is used to establish a secure connection between the two parties.

The SSL/TLS handshake is the process by which the client and server establish a secure connection. The handshake involves several steps, including:

1. Negotiating the protocol version
1. Verifying the certificate
1. Generating the encryption keys
1. Authenticating the client and server

The certificate is an essential component of the SSL/TLS protocol. The certificate contains information about the server's identity, including the server's public key, which is used to encrypt the data transmitted between the client and server. The certificate is signed by a trusted third-party organization, known as a Certificate Authority (CA), which verifies the server's identity.

## Certificate Handling in a Browser

When a client (such as a web browser) connects to a server, the browser checks the certificate to verify the server's identity. If the certificate is valid, the browser displays a padlock icon in the address bar, indicating that the connection is secure. If the certificate is not valid, the browser displays a warning, indicating that the connection may not be secure.

## Enter the Era of TLS

### TLS Versions 1.0, 1.1, 1.2 and 1.3

Transport Layer Security (TLS) is the successor to the Secure Sockets Layer (SSL) protocol, and it is the most widely used protocol for securing internet communications. There are several versions of the TLS protocol, including TLS 1.0, 1.1, 1.2, and 1.3.

TLS 1.0 was released in 1999 and is the first widely adopted version of the TLS protocol. It provides basic security features, but it has several security vulnerabilities that have been exploited by attackers.

TLS 1.1 was released in 2006 and addresses some of the security vulnerabilities in TLS 1.0. However, it is still considered to be relatively weak compared to later versions of the protocol.

TLS 1.2 was released in 2008 and is considered to be more secure than previous versions. It includes several security improvements, such as the ability to use stronger encryption algorithms, improved data integrity checks, and better protection against man-in-the-middle attacks.

TLS 1.3 was released in 2018 and is the most recent version of the TLS protocol. It provides significant security improvements over previous versions, including faster handshakes, better protection against eavesdropping, and the ability to use modern encryption algorithms.

Today, use TLS 1.3, for secure internet communications. Older versions of the protocol, such as TLS 1.0 and 1.1, should be disabled or phased out due to known [security vulnerabilities](https://en.wikipedia.org/wiki/Transport_Layer_Security).


## Renewal of SSL/TLS Certificates

SSL/TLS certificates have a limited lifespan and must be renewed periodically. The lifespan of an SSL/TLS certificate depends on the type of certificate and the certificate authority. Generally, SSL/TLS certificates are valid for one to two years and must be renewed annually.
Different Types of SSL/TLS Certificates

There are several different types of SSL/TLS certificates, including:
   
1. Domain Validation (DV) certificates
1. Organization Validation (OV) certificates
1. Extended Validation (EV) certificates
1. Wildcard certificates
1. Multi-domain (SAN) certificates

Each type of certificate offers different levels of validation and security, with EV certificates providing the highest level of security and trust.

In conclusion, SSL/TLS is an important security protocol that provides secure communications over the internet. By encrypting the data transmitted between a client and a server, SSL/TLS ensures the confidentiality and integrity of the data, even if it is intercepted in transit. The SSL/TLS certificate is an essential component of the SSL/TLS protocol, and the encryption algorithm used in the certificate depends on the client and server software and the desired level of security.