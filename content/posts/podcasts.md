---
title: "Podcasts"
date: 2020-08-12T17:42:55-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["podcast","mongodb","lifestyle","ybits"]
categories: ["general"]

lightgallery: true
---

I was featured in one of the MongoDB podcast series where we discussed how I got the idea of building [ybits.io](https://ybits.io/info). Further we discussed about [Mongodb GridFS](https://docs.mongodb.com/manual/core/gridfs/) and many other features. I hope you'll enjoy the conversation. Reach out for any feedback/questions/comments: <https://open.spotify.com/episode/3d6GcNm92NKOE5ebTgRLhr>
