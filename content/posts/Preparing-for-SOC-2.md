---
title: "Preparing for SOC 2 - Part 1"
date: 2020-01-01T16:34:13-05:00
draft: false
---

_*DISCLAIMER: all external links provided in this article have no affiliation to promotion either directly or indirectly.*_

> Life is worthwile if you share. _Jim Rohn_

If you're new to security governance, SaaS companies seek to publicly prove that their business model and service offerings that the data processed on behalf of the customer (or user) is processed with integrity, high trust of security, confidentiality. To do so, they will request an external firm to conduct an audit of the entire business to validate that it aligns with industry and or governance standards. One of the standard is SOC (which stands for **System and Organization Controls**). There is also ISO series and many more.

This article focuses on SOC standard; how to prepare for its audit, what is required to achieve its validation.
According to Wikipedia SOC is an audit conducted by an approved third party firm. In most cases, this audit is typically done on a yearly basis in most cases.  
Something to note, SOC itself does not have a set of framework or whatever, but it validates against the framework standard the company opted to use.

Before diving deep into the topic, it's important I cover some of the basics.
Thare about three types of SOC: SOC 1, SOC 2, SOC 3. The later is more of a simplified version of SOC 2. A SOC 1 Type 1 report is an independent snapshot of the organization's control landscape on a given day. A SOC 1 Type 2 report adds a historical element - see more on this [Wikipedia page](https://en.wikipedia.org/wiki/SSAE_16).  
Most Small-Medium Enterprises (SME) would seek for SOC 2, which is what is covered in this article (specially for SaaS providers).
SOC 2 mandates to test the security, availability, processing integrity, confidentiality, and privacy of an organization.

## Putting things together before the audit

Offen in well established companies, there are two types of audit: internal and external done in various line of businesses within the company. Things I have learned while leading this project is that there might be a lot of confusion, but the below info will help clear things up.

### Do you have a Security Program?

The first step when thinking about SOC Type I or II, is to establish a security program based on a recognized framework (could be NIST CSF, ISO, COBIT, etc). Some very focussed frameworks are HIPAA, PCI, SOX. The audit firm will test and evaluate your entity based on the implemented framework and the business offering.

### Preparation

The following check list should help while preparing for the audit.

* Conduct a Security Risk Assessment - document this in a Risk Assessment registry
* Create and document all ISMS Policies; most importantly: information security Policies (Information Secutity Policy, Softwared Developement Life Cycle policy, Vendor management policy, risk management policy just to name a few but the framework the business chose to use possible provides better guidance on what policies are needed. There isn't a set of standard number of ISMS policies, but depending on the framework being referenced.
* Time stamp every document.
* Perform an internal and External penetration test. Web pentest if the business is a software development firm. I recommend this to be done on time.

### Conducting the internal audit

The primary focus on preparing for an external audit, is making sure everything is in place. Some primarily needed items: Gather all ISM (information security management) policies and procedures. check if what the policy states is indeed implemented and best practices are effectively applied whener possible across the company. Address all gaps addentified during the Risk Assesment.

To provide a litle more clarity from the above, here is a typical example of an internal audit. Let's say an Information Security Policy states that all AD accounts may have at least x minimum number of letters, characters, and length. Verify that this true, take a screenshot of setting and save it somewhere.  
Another scenario will be around Business Continuity and Disaster Recory. There must be a documentation of every table top exercises, test performed of the business disaster recovery.

From the above, there are plenty of example and user stories but not all of them will apply to your firm, therefore an Risk Assement as well as the Security framework will provide guidance or if you have used an externam firm help with the internal audit....

Remember as mentioned earlier, if there isn't any documentation, that is a failure of the control in that particular area.

_**Why is it important to first conduct and internal audit?**_
SOC audit isn't about giving you time to fix or address all the gaps identified from  and durring Security Risk Assessment but instead only tests and validates whether the business have maintained and followed best practices.

### Conclusion

In summary, SOC 2 audit mainly focuses security, availability, processing integrity, confidentiality, and privacy of an organization. Implementing a security program can help as whom is responsible if this function, understands most of these requirements and how its point allign with the business.  
In part two, I will cover what to do during the audit, what is expected, and apply some of the methode I've used that helped me which I strongly believe with also help you succeed.
