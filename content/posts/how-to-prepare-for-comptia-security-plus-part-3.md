---
title: "How to Prepare for Comptia Security+ - Part 3"
date: 2023-01-20T12:32:57-05:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["career","privacy","security","online security"]
categories: ["Certification"]

lightgallery: true
---

You just nailed your CompTIA Security+, you're now a CIS guru. Congratulation!! But, this is just the start of your life long journey of being recognized in the industry.
You are required to maintain the status of your skills current. But What does that mean?

Maintaining your CompTIA Security+ certification is an essential step in staying current in the field of cybersecurity. This vendor-neutral certification, confirms an individual's knowledge of security practices and technologies, including risk management, incident response, and encryption.

To maintain your Security+ certification, you must complete Continuing Education (CE) requirements every three years. The CE program requires earning 50 Continuing Education Units (CEUs) within the three-year period. CompTIA offers different ways to earn CEUs such as training, attending events, participating in webinars, or creating content such as this one.

Alternatively, if you cannot fulfill the CEU requirements within the 3-year validity period, you must recertify by passing the current Security+ exam prior to the certification expiration date. This ensures that you have the knowledge and skills to stay current in the field of cybersecurity and are able to protect a company's network from cyber threats.
In addition to maintaining your Security+ certification, there are other benefits to earning CEUs and/or recertifying. For example, earning CEUs can help you stay current in the field of cybersecurity, which can lead to career advancement opportunities.  
Additionally, recertifying can demonstrate to employers that you are dedicated to staying current in the field, which can help you stand out from other job candidates.


You have to choose your path. Either by accumulating 50 CEUs, recertifying with the Security+ Exam, or earning a non-CompTIA security certification such as [CISSP (Certified Information Systems Security Professional)](https://www.isc2.org/Certifications/CISSP#), [CEH](https://www.eccouncil.org/programs/certified-ethical-hacker-ceh/), or one of the many [SANS Institute certifications](https://www.sans.org/).

Note that, CompTIA does offer ceertification which are higher, and way more then the Security+ certification: The CompTIA [PenTest+](https://www.comptia.org/certifications/pentest). I you pass this certification you automatically gained renewal of your Security+ ceertification validity. 

This rule works with other CompTIA certifications as well. A+ < Network+ < Security+ < PenTest+.  
Because I have A+, Network+, Security+ certification, renewing the Security+ only will automaticall renew my A+ and Network+ certification as well.


In conclusion, maintaining your CompTIA Security+ certification is an important step in staying current in the field of cybersecurity. By completing the CE requirements and recertifying prior to the certification expiration date, you can demonstrate to employers that you have the knowledge and skills to protect their network and protect it from cyber threats. Additionally, earning CEUs and recertifying can lead to career advancement opportunities. So, it is highly recommended to stay on top of your certifications and renew them on time.

For information about CompTIA Continuing education, check out the official CompTIA website: https://www.comptia.org/continuing-education

Part 1: [How to Prepare For CompTIA Security+ Exam](https://almb.me/posts/how-to-prepare-for-comptia-security-plus-part-1/)  
Part 2: [How to Prepare For CompTIA Security+ Exam - Part 2](https://almb.me/posts/how-to-prepare-for-comptia-security-plus-part-2/)

Follow me on Twitter: [@asnip8](https://twitter.com/@asnip8)