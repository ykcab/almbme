---
title: "Red Balloon Security SummerCON CFT Write Up: Happy Saving Customers"
date: 2019-06-18T20:37:39-04:00

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["cryptography"]
categories: ["Security Engineering"]

lightgallery: true
---
On June 14th & 15th, 2019 couple CTF enthusiast participated to solve the Red Balloon Security CTF challenge at [SummerCON](https://www.summercon.org/) announced [here](https://twitter.com/redballoonsec/status/1139286380939534336). For those who aren't familiar with SummerCON, it's a small 2-day hackers' conference organized and led by volunteers in New York City. Think of its culture similar to [DEFCON](https://defcon.org/).  

To kick off the game, each player collects two ATM card: a debit and a credit card. The tasks are **to find the flag to load money into the issued ATM cards**. Each flag has to be submitted in a form of `http://10.0.0.2:8080/<flag.html>`.  
There were only three levels but required some strategic thinking to go through them.

Flag 1: **w1ndows** - difficulty 1/5

The debit card has to be re-programmed.  
The solution was to decode hex encoded value that was on the home page `http://10.0.0.2:8080/readme.html`. Below is the hex value, and you can decode it [here](https://convertstring.com/EncodeDecode/HexDecode):

```446f2079 6f75206b 6e6f7720 74686973 2041544d 2072756e 73206f6e 2057696e 646f7773 20434520 362e303f 2077316e 646f7773 5f63335f 72756c65 7a2e6874 6d6c```

After successfully decoded the above hex string, the next step is to insert the flag into this URL `http://10.0.0.2:8080/<flag.html>`, go to the page to get a 16 digit number for the debit card re-programing. A card reader was provided by Red Balloon Security. This allowed players to reprogram the debit card.
Once the card has been programmed with the new number, automatically $11 is awareded and load into the card which can be withdrawn at the ATM. Yeeh, Free Lunch!!! 
But, you can only withdraw $10 beacuse there is $1 transaction fee.

Level 2 - *ST3GO* - (difficulty: 2.5/5).   
The hint (below) for this flag has a similarity with [this quote](https://en.wikipedia.org/wiki/One_Ring) by J.R. R Tolkien from "The One Ring":

> One Code Rule them all, One Code Find them All,  
> Once Code to bring them Together,  
> and one Character to bind them.  
> What Code am I?"

Got a hint? Drop me a message!

More to come soon, stay tuned!
