---
title: "Summer Cryptography Journal 2020"
date: 2020-06-24T22:03:08-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["cryptography"]
categories: ["Security Engineering"]

lightgallery: true
---


Over the past couple of weeks I  took some time to research  for a feasible cryptographic primitive protocol that can be used in [YBITS.IO](https://ybits.io), a secure, simplefied, ananonymous large file tranfer service.
Below is compilation of some blog posts and research papers I found to be useful that may also help you on your next project.

For a more valuable, in depth resources check out the IACR database <https://eprint.iacr.org/>

### RFC & Scientific papers

* EME Encryption <https://eprint.iacr.org/2003/147.pdf> and a golang implementation can be found at this link <https://github.com/rfjakob/eme#eme>
* SIV Encryption   <https://tools.ietf.org/html/rfc5297>
* AES-GCM-SIV     <https://tools.ietf.org/html/rfc8452>
* Online SIV aka eSTREAM SIV or (OAE) <https://eprint.iacr.org/2015/189.pdf>
* Zero knowledge Proofs:
    - <a href="https://datatracker.ietf.org/doc/rfc8235/?include_text=1>" target="_blank" rel="noopener"><span>Schnorr Non-interactive Zero-Knowledge Proof (IETF RFC)</span> </a>
    - <a href="https://blog.decentriq.ch/zk-snarks-primer-part-one/" target="_blank" rel="noopener"><span>Introduction to zk-SNARKs</span> </a>

* ChaCha20Poly1305 <https://tools.ietf.org/html/rfc7539>

You'd ask why isn't there AES-GCM? well, jump into this <https://soatok.blog/2020/05/13/why-aes-gcm-sucks/>. But that not the actual reason, the point is many of the listed chryptographic schemes here are less known to developers.
Addintionally, [Homomorphic Encryption](https://www.microsoft.com/en-us/research/project/homomorphic-encryption/) is gaining wider adoption in cloud. This is something to keep an eye on.

### Blog Posts

* <https://soatok.blog/tag/cryptography/> - a series of opinionated posts based on differents Cryptography scheme, their usage and level of security.
* <https://moxie.org/2011/12/13/the-cryptographic-doom-principle.html> - an opinionated post about "Cryptographic Doom Principle".
* Zcash Zero Knowledge proof <https://z.cash/technology/zksnarks/>
* Encrypting Streams <https://www.imperialviolet.org/2014/06/27/streamingencryption.html>

Now, for not reinventing the wheel, here is another great compilation of cryptographic protocol source code in almost every popular language <https://github.com/sobolevn/awesome-cryptography>

Do you have suggestions or typo to report? Reach out via twitter [@asnip8](https://twitter.com/asnip8) or submit a [PR](https://gitlab.com/ykcab/almbme)
