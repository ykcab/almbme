---
title: "Certbot Standalone SSL Update/Install"
date: 2021-04-11T17:33:27-04:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["certbot"]
categories: ["Engineering"]

lightgallery: true
---

Sometimes you just want to do things a bit different such as testing your developement locally with a production SSL certifcate. Of course you can use [this guide by Filippo](https://github.com/FiloSottile/mkcert) about configuring HTTPS on your localhost. But, thanks to Let's Encrypt which you can get an SSL certificate with no hustle.

This guide is straight forward but the only requirement is to have access to your DNS registrar to update the `TXT _acme_` validation record.

```bash
Step 1. Install certbot on your working dev computer as a normal user
Step 2. Run the following using normal user (non-adminstrative account) and follow the pompt:
    sudo certbot certonly --manual  --preferred-challenges dns
Step 3. and the your DNS records & Copy the TXT _acme challenge record prompted and   past it to your registrar.
Step 4. Wait for about 5-10 seconds after updating the TXT record. Return to the terminal and press enter.
Step 5. That should be it.
```
