---
title: "Mongodb Field Level Encryption Overview"
date: 2020-08-15T11:09:02-04:00
draft: true

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["cryptography","mongodb"]
categories: ["Security Engineering"]

lightgallery: true
---

## Geting started

Let start with Mongo Atlas:
-you need an Atlas account - create now for free <- add signup link
-install mongocrypd binary
-follow instruction in the documentation ? not really clear though
   install mongocryptd: from this commands:
   `echo "deb [ arch=amd64,arm64,s390x ] http://repo.mongodb.com/apt/ubuntu focal/mongodb-enterprise/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-enterprise.list`
   `sudo apt-get update`
   `supd apt-get mongodb-enterprise-cryptd` <- notice only the mongocryptd that is being installed
Requirement:
Ubuntu 20.04 LTS
Mongodb 4.2

let's follow this example:
https://github.com/mongodb-labs/field-level-encryption-sandbox/blob/master/go/main.go

this one requires Mongo Enterprise or atlas ???
