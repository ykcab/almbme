---
title: "What Password Manager to Use in 2023"
date: 2022-12-14T08:35:11-05:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["security","enncryption", "consumer security"]
categories: ["Information Security"]

lightgallery: true
---

Passwords are an important part of our online lives. They keep our personal information secure and protect our accounts from unauthorized access. But with so many different online accounts and services, it can be difficult to keep track of all of our passwords. This is where a password manager comes in.

**A password manager** is a tool that allows you to store all of your passwords in one secure location. This means that you only have to remember one master password to access all of your other passwords. This makes it much easier to manage your passwords and keep them secure without writing them down on a paper notebook or an Excel spreadsheet.

### How It Works

A password manager works by storing all of your passwords in one secure location. This is typically a database or a cloud-based service that is encrypted and protected by a master password. When you want to access one of your passwords, you enter your master password to unlock the password manager. This allows you to view and use your stored passwords.

Most password managers also have a number of additional features, such as a password generator and the ability to automatically fill in your login information on websites. Some password managers also have additional security features, such as two-factor authentication or a virtual private network (VPN) to add an extra layer of protection.

### Master This...

One of the biggest advantages of using a password manager is that it can help you create strong, unique passwords for all of your accounts. Most password managers have a password generator tool that can create complex, randomized passwords for you. These passwords are much harder to guess or crack than simple, easily-guessed passwords like "password" or "123456."

Another advantage of using a password manager is that it can automatically fill in your username and password for you when you visit a website. This means that you don't have to remember each password individually, and you can log in to your accounts quickly and easily.

In addition to making it easier to manage your passwords, a password manager can also help keep your personal information secure. Most password managers use encryption to protect your passwords, which means that they are scrambled and unreadable to anyone who doesn't have the correct decryption key. This added layer of security can help prevent your passwords from being stolen or accessed by unauthorized users.

### The Best Password Manager in 2023

_**THE EDITOR'S PICK**:_ [Bitwarden](#the-best-password-manager-in-2023)

Update Dec 23, 2022: There have been a lot of scrutinities aroound Lastpass with [the recent hack](https://www.tomsguide.com/news/lastpass-hack-was-even-worse-than-originally-reported-should-you-delete-your-account). In most cases, I don't recommend it but it worth the mention in this post.

1. [Bitwarden](https://bitwarden.com): One of the key advantages of Bitwarden is that it is open-source and free to use. This means that anyone can access the source code and verify that it is secure and does not contain any vulnerabilities. Here is their recent [SOC 2 audit report](https://bitwarden.com/blog/bitwarden-achieves-soc-2-certification/). It is also available on a wide range of platforms, including Windows, Mac, Linux, iOS, and Android, making it a convenient option for users with multiple devices.
I strongly recommend Bitwarden as it is a solid password manager that offers many of the same features as other popular password managers, but is available for free. It is a good option for users who are looking for an open-source and secure password manager.

1. [1Password](https://1password.com): 1Password is another popular password manager that offers many of the same features as LastPass. It has a password generator, can automatically fill in your login information, and uses encryption to protect your passwords. 1Password is great for Enterprise use (but it doesn't mean you cannot use it for personal and family use).  
**_July 11th, 2023. IMPORTANT NOTE_**: If privacy is something you care about, it was reported that 1Password has tracking and telemetry [1](https://themarkup.org/blacklight?url=1password.com) [2](https://twitter.com/iAnonymous3000/status/1678435851141881856?s=20). Not that this would be a deal breaker in my opinion but this is something to keep in mind, and I still recommend it if Bitwarden isn't intuitive to you.

1. [Dashlane](https://www.dashlane.com/): Dashlane is a password manager that offers a number of advanced features, including the ability to securely share passwords with others and access to a virtual private network (VPN) for added security. It also has a password generator and can automatically fill in your login information.

1. [KeePass](https://keepass.info/): KeePass is an open-source password manager that is free to use. It allows you to store all of your passwords in one secure location, and it has a password generator to help you create strong, unique passwords. It also has a number of advanced features for more advanced users. The sellout of KeepPass, it is portable. Which means you can store the database on a portable memory stick.  
But the downside is you must make sure you don't lose that portable storage drive.

1. [LastPass](https://lastpass.com): This used to be my favorite but because of the user policy changes introduced about a year ago, it was against what I believe in and support. Although, worth to mention their auto fill feature is the best compared to the other listed above.

### Write up

Using a password manager is an important part of keeping your online accounts and personal information secure. It can help you create strong, unique passwords, make it easier to manage your passwords, and add an extra layer of security to protect your personal information. Whether you're a casual internet user or a power user with dozens of online accounts, a password manager can help you stay safe and secure online.

If you enjoyed reading this, check out my previous article about webauthn.


