---
title: "Quickstart Golang-Mongodb: A Quick Look at Gridfs"
date: 2019-12-19T13:53:53-05:00
draft: false

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["mongodb","ybits","git"]
categories: ["Engineering"]

lightgallery: true
---

A guest post I wrote on MongoDB developer blog. I hope you'll enjoy it.
[Quickstart Golang-Mongodb: A Quick Look at Gridfs](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--a-quick-look-at-gridfs)