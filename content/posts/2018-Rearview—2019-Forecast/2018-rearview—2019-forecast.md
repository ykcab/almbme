---
title: "2018 Rearview—2019 Forecast"
date: 2019-06-17T21:40:24-04:00
author: "alain"
authorLink: "https://almb.me/about"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["lifestyle"]
categories: ["general"]

lightgallery: true
---
_Note: post originally published on Januray 4th, 2019_  
> _2018 was a year of observations and lots of trials, but more importantly a year of freedom_  

The year two thousand and eighteen was one of those years where I risked it all. I’ve never written a year review as I’ve never seen the need for it. This year, I was compelled by the love and gratitude of how much has happened is miserable. I experienced freedom is in many areas of my existence.  
The key areas I will talk about will be Professional Development, Personal Growth, and Community.Alright, the key areas I will talk about will be Professional Development, Personal Growth, and Community.

**Professional development**  
I became more engaged in the industry than before. Faith, dedication and determination led me to a brighter yet amazing career vision. I finally find myself doing what I love and it has been amazing ever since.
I moved to New York City to join our amazing security team 🛫.  
I gave a talk and mentored at [Meetup](https://learnteachcode.org/)  
I gave a talk on Data Breaches at [Tech Symposium Cal Poly](https://techsymposium.calpolyswift.org/#schedule). If you’re in the Los Angeles area, I highly recommend to support this mission.
Attended conferences, played and won a fun contest.  

**Personal Growth**  
“Success is not the key to happiness. Happiness is the key to success. If you love what you are doing, you will be successful.”  — Albert Schweitzer  
I met a friend at a social Meetup event who introduced me to Landmark. During our conversation, I was moved, inspired and touched by the energy and confidence she had about her life, goals, relationships. She recommended me to check out the introductory session. I accepted the invitation. Fast forward, after that introductory session, I enrolled in their “Landmark Forum” which was scheduled to be held in Culver City in February 2018. Since then, I never look at the world the same again. There has been a transformation in my relationships with family, friends and most importantly with God. And I am grateful 🙏  

Futher, I finally got the courage to read more non-technical books, weird ones. Here is a list of some inspirational books that shaped my perspectives:  

Non-Tech:  

Secrets of Millionaire Mind -T. Hav Eker  
Who Moved My Pulpit? Leading Change in the Church -Thom S. Rainer  
Your Next 24 Hours - Hal Donaldson and Kirk Noonan  

Tech books:  

RTM  - Red Team Field Manual  
BTFM - Blue Team Field Manual  
Ahead in the Cloud. Best Practices for Navigating the Future of the Enterprise IT - Stephen Orban  
Cryptography Engineering — Niels Ferguson, Bruce Schneier, Tadayoshi Kohno

**Community**  
The main engagement I had this year is serving at my local churches as a sound engineer in Los Angeles and now in New York.  

**The Forecast - 2019 and ahead**
_Consistency and Love will be the goals I set for 2019_  

Consistency /n/: a degree of density, firmness -harmony.

Devotion to what matters the most for my well being: daily meditation, physical exercises.  
Become an SME by acquiring skills needed to advance my career, never waste time on what’s not important 😇  
Contribute to OSS (Open Source Software) projects.  

And with that, nothing is ever easy, nothing is impossible either. There will be breakdowns along the way, and that’s OK! 2019 is a year of positiveness.

If you liked this post, share and follow me on Twitter.  Until then #secureallthethings  
