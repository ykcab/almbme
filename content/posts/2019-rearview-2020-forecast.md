---
title: "2019 Rearview 2020 Forecast"
date: 2019-12-31T11:59:32-05:00
draft: true

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["lifestyle"]
categories: ["general"]

lightgallery: true
---

# 2019 Rearview - 2020 Forecast

# 2019

What had happened?

Since I started (actively) blogging, I set a custom to reflect on some of the achievements I accomplished through out the year. My first Rearview-Forecast was last last year (2019). It's very inpirational and, I'd recommend you check it out [here](https://almb.me/post/2018-rearview2019-forecast/).

Each year, as per tradition, I set a moto of the year. 2019 was year of positiveness. Let break it down how this all unfollowed.

In a nutshell, this what happened
- I finaly setup this blog. You can read more in-depth of the setup [here](https://almb.me/post/moving-to-gitlab-pages/)
- I landed a Security Engineer role: This was one of the goals I set for 2019. I couldn't be happier and it is just the beginint of what more to come.
- I didn't write as much as I hoped too this year but one the bigest achievement was giving back. From technical contribution to charity contribution to spiritual growth support. [This post](https://www.mongodb.com/blog/post/quick-start-golang--mongodb--a-quick-look-at-gridfs) is one of which I am really proud of.

# 2020
This year, is a year of *realisation*. I have broathen up this year's focus as to not limit myself to on set of direction. 2019 