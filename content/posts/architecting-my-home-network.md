---
title: "Home Network Re-Architecting - Joining the NYC Mesh"
date: 2019-06-20T22:40:01-04:00

author: "alain"
authorLink: "https://almb.me"

resources:
  - name: ""
    src: ""
  - name: ""
    src: ""

tags: ["Networking"]
categories: ["Engineering"]

lightgallery: true
---

In December 2019 [I published an article](https://almb.me/post/2018-rearview2019-forecast/) which elaborates a little of what I’ll focus on in 2019. I had not taken the courage to write often, but I am glad to get back to it.

This quarter, the focus was on servicing and giving back as shared. As part of contributing and giving back to community, I joined the [NYC Mesh](https://www.nycmesh.net/) community network and dished out the tradition ISP because, why not. NYC Mesh is a not-for-profit community network runs by volunteers.

A while back I stumbled on an article which mentioned their existence, and it caught my attention. I was living in a different state at the time. When I moved to NYC, it was about time to check them out.

Why mesh network? I was curious about their mission and service delivery compare to traditional ISP. More of I was looking for a provider with a better customer service. Unfortunately if you’re not a Tier 1 or Tier 2 business owner, support from a traditional ISP is a nightmare, and not to mention the throttling and ads injections.

So far, the journey has been great and the community is scaling rapidly with an average of 2 installations per day. See the interactive map at this link https://www.nycmesh.net/map. The treat you get form the support (volunteer) team can’t be compared to your traditional ISP. NYC Mesh is the best in customer service. With that,thanks to everyone for their time and support, and shout out everyone supporting every members.

### How to Join the Mesh Network?

As the network expends, there are still some neighbourhood with no line of sight or LoS. A line of sight is the probable distance of the wireless propagration
Next, I will dive in the actual architecture of the my home network leveraging microservices.

Stay tuned …✌️
